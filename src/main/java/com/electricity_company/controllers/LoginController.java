package com.electricity_company.controllers;

import com.electricity_company.entities.User;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {
    @PreAuthorize("!isAuthenticated()")
    @GetMapping("/login")
    public String showLogin(@RequestParam(required = false) String error, Model model, User user) {
        if (error != null) {
            model.addAttribute("error", "Wrong login information!");
        }

        return "login";
    }
}
