package com.electricity_company.controllers.models;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.electricity_company.entities.MeasurementStatus;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementViewModel {
    private long id;
    private MeasurementStatus status;
    private BigDecimal price;
    private BigDecimal amount;
    private LocalDate measuredOn;
}