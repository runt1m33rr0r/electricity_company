package com.electricity_company.controllers.models;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientViewModel extends UserViewModel {
    @NotNull
    private BigDecimal highestPayment;

    @NotNull
    private BigDecimal amountPaid;

    @NotNull
    private List<MeasurementViewModel> measurements;
}