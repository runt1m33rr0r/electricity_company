package com.electricity_company.controllers.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileViewModel {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}