package com.electricity_company.controllers.models;

import java.math.BigDecimal;

import lombok.*;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillsViewModel {
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private BigDecimal profitBill;
}