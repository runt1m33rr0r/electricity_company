package com.electricity_company.controllers.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserViewModel {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
}