package com.electricity_company.controllers.models;

import java.math.BigDecimal;

import lombok.*;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PricesViewModel {
    @DecimalMin(value = "0")
    private BigDecimal privateIndividualPrice;

    @DecimalMin(value = "0")
    private BigDecimal legalEntityPrice;
}