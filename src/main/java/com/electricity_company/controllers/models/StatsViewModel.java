package com.electricity_company.controllers.models;

import java.math.BigDecimal;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatsViewModel {
    private BigDecimal income;
    private BigDecimal spending;
    private BigDecimal profit;
    private BigDecimal pureProfit;
}