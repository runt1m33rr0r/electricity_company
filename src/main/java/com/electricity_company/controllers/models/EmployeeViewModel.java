package com.electricity_company.controllers.models;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeViewModel extends UserViewModel {
    @DecimalMin(value = "0")
    private BigDecimal salary;
}