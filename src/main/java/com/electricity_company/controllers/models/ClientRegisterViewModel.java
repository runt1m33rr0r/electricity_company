package com.electricity_company.controllers.models;

import javax.validation.constraints.NotNull;

import com.electricity_company.entities.ClientType;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientRegisterViewModel extends UserRegisterViewModel {
    @NotNull
    private ClientType clientType;
}