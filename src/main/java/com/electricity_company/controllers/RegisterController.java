package com.electricity_company.controllers;

import javax.validation.Valid;

import com.electricity_company.controllers.models.*;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.*;
import com.electricity_company.utils.contracts.IMappingUtils;
import com.electricity_company.exceptions.InvalidDataException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class RegisterController {
    private final IClientsService clientsService;
    private final IEmployeesService employeesService;
    private final IAdminService adminService;
    private final IMappingUtils mapper;

    @PreAuthorize("isAuthenticated() && hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping("/clients/register")
    public String showClientRegister(@ModelAttribute("user") ClientRegisterViewModel user) {
        return "register-client";
    }

    @PreAuthorize("isAuthenticated() && hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @PostMapping("/clients/register")
    public String registerClient(Model model, @Valid @ModelAttribute("user") ClientRegisterViewModel user,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register-client";
        }

        CreateClientServiceModel serviceModel = this.mapper.map(user, CreateClientServiceModel.class);
        try {
            this.clientsService.createClient(serviceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return "register-client";
        }

        model.addAttribute("success", "Successfully registered!");

        return "register-client";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @GetMapping("/employees/register")
    public String showEmployeeRegister(@ModelAttribute("user") EmployeeRegistrationViewModel user) {
        return "register-employee";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping("/employees/register")
    public String registerEmployee(Model model, @Valid @ModelAttribute("user") EmployeeRegistrationViewModel user,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register-employee";
        }

        EmployeeServiceModel serviceModel = this.mapper.map(user, EmployeeServiceModel.class);
        try {
            this.employeesService.createEmployee(serviceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return "register-employee";
        }

        model.addAttribute("success", "Successfully registered!");

        return "register-employee";
    }

    @PostMapping("/admins/register")
    public String registerAdmin(Model model, @Valid @ModelAttribute("user") AdminRegisterViewModel admin,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register-admin";
        }

        try {
            AdminServiceModel serviceModel = this.mapper.map(admin, AdminServiceModel.class);
            this.adminService.createAdmin(serviceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return "register-admin";
        }

        model.addAttribute("success", "Admin created!");

        return "register-admin";
    }
}
