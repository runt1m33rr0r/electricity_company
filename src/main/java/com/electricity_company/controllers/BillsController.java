package com.electricity_company.controllers;

import javax.validation.Valid;

import com.electricity_company.controllers.models.BillsViewModel;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.IBillsService;
import com.electricity_company.services.models.BillsServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/bills")
@AllArgsConstructor
public class BillsController {
    private final IMappingUtils mapper;
    private final IBillsService billsService;

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @GetMapping()
    public String getBills(Model model) {
        BillsViewModel bills = this.mapper.map(this.billsService.getBills(), BillsViewModel.class);
        model.addAttribute("bills", bills);

        return "bills";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping()
    public String setBills(Model model, @Valid @ModelAttribute("bills") BillsViewModel bills,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "bills";
        }

        try {
            this.billsService.setBills(this.mapper.map(bills, BillsServiceModel.class));
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.getBills(model);
        }

        model.addAttribute("success", "Bills updated!");

        return this.getBills(model);
    }
}