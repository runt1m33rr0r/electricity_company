package com.electricity_company.controllers;

import java.util.Collection;

import javax.validation.Valid;

import com.electricity_company.controllers.models.PropertyViewModel;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.IPropertiesService;
import com.electricity_company.services.models.PropertyServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/properties")
@AllArgsConstructor
public class PropertyController {
    private final IMappingUtils mapper;
    private final IPropertiesService propertiesService;

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @GetMapping("/create")
    public String getCreateProperty(Model model, @ModelAttribute("property") PropertyViewModel property) {
        return "create-property";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping("/create")
    public String createProperty(Model model, @Valid @ModelAttribute("property") PropertyViewModel property,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "create-property";
        }

        PropertyServiceModel propertyServiceModel = this.mapper.map(property, PropertyServiceModel.class);
        try {
            this.propertiesService.createProperty(propertyServiceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return "create-property";
        }

        model.addAttribute("success", "Property added!");

        return "create-property";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @GetMapping()
    public String getProperties(Model model) {
        Collection<PropertyViewModel> properties = this.mapper.mapAll(this.propertiesService.getAll(),
                PropertyViewModel.class);
        model.addAttribute("properties", properties);

        return "manage-properties";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping("/delete")
    public String deleteProperty(Model model, @ModelAttribute("propertyAddress") String address) {
        try {
            this.propertiesService.deleteProperty(address);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.getProperties(model);
        }

        model.addAttribute("success", "Property deleted!");

        return this.getProperties(model);
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping("/edit")
    public String editProperty(Model model, @Valid @ModelAttribute("editProperty") PropertyViewModel property,
            BindingResult bindingResult) {
        PropertyServiceModel propertyServiceModel = this.mapper.map(property, PropertyServiceModel.class);
        try {
            this.propertiesService.editProperty(propertyServiceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.getProperties(model);
        }

        model.addAttribute("success", "Property updated!");

        return this.getProperties(model);
    }
}