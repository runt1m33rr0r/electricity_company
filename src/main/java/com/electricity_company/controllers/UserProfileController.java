package com.electricity_company.controllers;

import javax.validation.Valid;

import com.electricity_company.controllers.models.UserProfileViewModel;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.UserServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/profile")
@AllArgsConstructor
public class UserProfileController {
    private final IMappingUtils mapper;
    private final IAuthService authService;
    private final IClientsService clientsService;

    @PreAuthorize("isAuthenticated()")
    @GetMapping()
    public String showUserProfile(Model model, @ModelAttribute("user") UserProfileViewModel user) {
        UserProfileViewModel loggedInUser = this.mapper.map(this.authService.getLoggedInUser(),
                UserProfileViewModel.class);
        loggedInUser.setPassword("");
        model.addAttribute("loggedInUser", loggedInUser);

        return "profile";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping()
    public String changeProfile(Model model, @Valid @ModelAttribute("user") UserProfileViewModel user) {
        UserProfileViewModel loggedInUser = this.mapper.map(this.authService.getLoggedInUser(),
                UserProfileViewModel.class);
        loggedInUser.setPassword("");
        model.addAttribute("loggedInUser", loggedInUser);

        try {
            UserServiceModel userServiceModel = this.mapper.map(user, UserServiceModel.class);
            this.authService.modifyUser(loggedInUser.getEmail(), userServiceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return showUserProfile(model, user);
        }

        model.addAttribute("success", "Successfully changed profile data!");

        return showUserProfile(model, user);
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_CLIENT')")
    @PostMapping("/delete")
    public String deleteClient(Model model) {
        UserProfileViewModel loggedInUser = this.mapper.map(this.authService.getLoggedInUser(),
                UserProfileViewModel.class);
        loggedInUser.setPassword("");
        model.addAttribute("loggedInUser", loggedInUser);

        try {
            this.clientsService.deleteClient(loggedInUser.getEmail());
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return "profile";
        }

        return "redirect:/logout";
    }
}
