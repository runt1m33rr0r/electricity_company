package com.electricity_company.controllers;

import java.util.ArrayList;

import javax.validation.Valid;

import com.electricity_company.controllers.models.*;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.IMeasurementsService;
import com.electricity_company.services.models.CreateMeasurementServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/measurements")
@AllArgsConstructor
public class MeasurementController {
    private final IMeasurementsService measurementsService;
    private final IMappingUtils mapper;

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_EMPLOYEE')")
    @GetMapping("/create")
    public String getCreateMeasurementPage(Model model,
            @ModelAttribute("measurement") CreateMeasurementViewModel measurement) {
        return "measurement";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_EMPLOYEE')")
    @PostMapping("/create")
    public String createMeasurement(Model model,
            @Valid @ModelAttribute("measurement") CreateMeasurementViewModel measurement, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "measurement";
        }

        try {
            this.measurementsService
                    .createMeasurement(this.mapper.map(measurement, CreateMeasurementServiceModel.class));
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return "measurement";
        }

        model.addAttribute("success", "Measurement created!");

        return "measurement";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_CLIENT')")
    @GetMapping()
    public String getLoggedInClientMeasurements(Model model) {
        try {
            model.addAttribute("measurements", this.mapper
                    .mapAll(this.measurementsService.getLoggedInClientMeasurements(), MeasurementViewModel.class));
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            model.addAttribute("measurements", new ArrayList<MeasurementViewModel>());
        }

        return "client-measurements";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_CLIENT')")
    @PostMapping("/pay")
    public String payMeasurement(Model model, @ModelAttribute("measurementId") String measurementId) {
        try {
            this.measurementsService.payMeasurement(Long.parseLong(measurementId));
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.getLoggedInClientMeasurements(model);
        }

        model.addAttribute("success", "Measurement paid!");

        return this.getLoggedInClientMeasurements(model);
    }
}