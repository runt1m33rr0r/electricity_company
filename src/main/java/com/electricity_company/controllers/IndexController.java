package com.electricity_company.controllers;

import com.electricity_company.controllers.models.AdminRegisterViewModel;
import com.electricity_company.services.contracts.IAuthService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class IndexController {
    private final IAuthService authService;

    @RequestMapping("/")
    public String showIndex(Model model, @ModelAttribute("user") AdminRegisterViewModel admin) {
        if (!this.authService.adminExists()) {
            return "register-admin";
        }

        String title = "Report system";
        model.addAttribute("title", title);

        return "index";
    }

    @GetMapping("/denied")
    public String showDeniedPage() {
        return "denied";
    }
}
