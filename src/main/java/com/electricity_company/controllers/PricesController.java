package com.electricity_company.controllers;

import javax.validation.Valid;

import com.electricity_company.controllers.models.PricesViewModel;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.IPricesService;
import com.electricity_company.services.models.PricesServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/prices")
@AllArgsConstructor
public class PricesController {
    private final IMappingUtils mapper;
    private final IPricesService pricesService;

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @GetMapping()
    public String getPrices(Model model) {
        PricesViewModel prices = this.mapper.map(this.pricesService.getPrices(), PricesViewModel.class);
        model.addAttribute("prices", prices);

        return "prices";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping()
    public String setPrices(Model model, @Valid @ModelAttribute("prices") PricesViewModel prices,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "prices";
        }

        try {
            this.pricesService.setPrices(this.mapper.map(prices, PricesServiceModel.class));
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.getPrices(model);
        }

        model.addAttribute("success", "Prices updated!");

        return this.getPrices(model);
    }
}