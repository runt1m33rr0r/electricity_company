package com.electricity_company.controllers;

import java.math.BigDecimal;
import java.util.*;

import com.electricity_company.controllers.models.ClientViewModel;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.IClientsService;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/clients")
@AllArgsConstructor
public class ClientController {
    private final IClientsService clientsService;
    private final IMappingUtils mapper;

    @PreAuthorize("isAuthenticated() && hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping()
    public String showClientsManagement(Model model,
            @RequestParam("maximumPayment") Optional<BigDecimal> maximumPayment) {
        List<ClientViewModel> clients = this.mapper.mapAll(this.clientsService.getAllClients(maximumPayment),
                ClientViewModel.class);
        model.addAttribute("clients", clients);

        return "manage-clients";
    }

    @PreAuthorize("isAuthenticated() && hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @PostMapping("/delete")
    public String deleteClient(Model model, @ModelAttribute("clientEmail") String email) {
        try {
            this.clientsService.deleteClient(email);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.showClientsManagement(model, Optional.empty());
        }

        model.addAttribute("success", "Client deleted!");

        return this.showClientsManagement(model, Optional.empty());
    }
}
