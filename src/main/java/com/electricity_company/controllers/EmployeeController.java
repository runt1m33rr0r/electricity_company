package com.electricity_company.controllers;

import java.util.Collection;

import javax.validation.Valid;

import com.electricity_company.controllers.models.*;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.IEmployeesService;
import com.electricity_company.services.models.EditEmployeeServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/employees")
@AllArgsConstructor
public class EmployeeController {
    private final IEmployeesService employeesService;
    private final IMappingUtils mapper;

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @GetMapping()
    public String showEmployeesManagement(Model model) {
        model.addAttribute("employee", new EditEmployeeViewModel());

        Collection<EmployeeViewModel> employees = this.mapper.mapAll(this.employeesService.getAll(),
                EmployeeViewModel.class);
        model.addAttribute("employees", employees);

        return "manage-employees";
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping("/delete")
    public String deleteEmployee(Model model, @ModelAttribute("employeeEmail") String email) {
        try {
            this.employeesService.deleteEmployee(email);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.showEmployeesManagement(model);
        }

        model.addAttribute("success", "Employee fired!");

        return this.showEmployeesManagement(model);
    }

    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    @PostMapping("/edit")
    public String editEmployee(Model model, @Valid @ModelAttribute("employee") EditEmployeeViewModel employee,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().get(0).getDefaultMessage());

            return this.showEmployeesManagement(model);
        }

        try {
            EditEmployeeServiceModel editEmployeeServiceModel = this.mapper.map(employee,
                    EditEmployeeServiceModel.class);
            this.employeesService.editEmployee(editEmployeeServiceModel);
        } catch (InvalidDataException e) {
            model.addAttribute("error", e.getMessage());

            return this.showEmployeesManagement(model);
        }

        model.addAttribute("success", "Employee updated!");

        return this.showEmployeesManagement(model);
    }
}
