package com.electricity_company.controllers;

import com.electricity_company.controllers.models.StatsViewModel;
import com.electricity_company.services.contracts.IStatsService;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class StatsController {
    private final IStatsService statsService;
    private final IMappingUtils mapper;

    @PreAuthorize("isAuthenticated() && hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping("/stats")
    public String getStats(Model model) {
        StatsViewModel statsViewModel = this.mapper.map(this.statsService.getStats(), StatsViewModel.class);
        model.addAttribute("stats", statsViewModel);

        return "stats";
    }
}