package com.electricity_company.services;

import java.util.*;

import com.electricity_company.config.MyUserPrincipal;
import com.electricity_company.entities.*;
import com.electricity_company.repositories.*;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.UserServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;
import com.electricity_company.exceptions.InvalidDataException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AuthService implements IAuthService {
    private final IMappingUtils mapper;
    private final UsersRepository usersRepository;
    private final RolesRepository rolesRepository;
    private final IValidationService validator;
    private final PasswordEncoder passwordEncoder;

    private MyUserPrincipal getUserPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserPrincipal userPrincipal = (MyUserPrincipal) authentication.getPrincipal();

        return userPrincipal;
    }

    @Override
    public User registerUser(UserServiceModel user, Collection<String> roles) throws InvalidDataException {
        if (user == null) {
            throw new InvalidDataException("No user data provided!");
        }

        if (roles == null || roles.isEmpty()) {
            throw new InvalidDataException("Invalid user roles!");
        } else {
            for (String role : roles) {
                if (role == null || role == "") {
                    throw new InvalidDataException("Invalid user role!");
                }
            }
        }

        if (this.usersRepository.existsByEmail(user.getEmail())) {
            throw new InvalidDataException("User already exists!");
        }

        this.validator.validate(user);

        User userEntity = this.mapper.map(user, User.class);
        this.validator.validate(userEntity);

        List<UserRole> userRoles = new ArrayList<>();
        for (String role : roles) {
            UserRole currentRole = new UserRole(role);
            this.validator.validate(currentRole);

            if (!this.rolesRepository.existsByName(role)) {
                this.rolesRepository.save(currentRole);
            } else {
                currentRole = this.rolesRepository.findFirstByName(role);
            }

            userRoles.add(currentRole);
        }

        userEntity.setRoles(userRoles);
        userEntity.setPassword(this.passwordEncoder.encode(userEntity.getPassword()));
        this.usersRepository.save(userEntity);

        return userEntity;
    }

    @Override
    public void deleteUser(String email) throws InvalidDataException {
        if (email == null) {
            throw new InvalidDataException("Invalid data!");
        }

        User existingUser = this.usersRepository.findFirstByEmail(email);
        if (existingUser == null) {
            throw new InvalidDataException("User does not exist!");
        }

        existingUser.setDeleted(true);
        this.usersRepository.save(existingUser);
    }

    @Override
    public void modifyUser(String email, UserServiceModel newUser) throws InvalidDataException {
        if (email == null || newUser == null) {
            throw new InvalidDataException("Invalid input!");
        }

        User existingUser = this.usersRepository.findFirstByEmail(email);
        if (existingUser == null) {
            throw new InvalidDataException("User does not exist!");
        }

        MyUserPrincipal userPrincipal = this.getUserPrincipal();
        if (newUser.getEmail() != null && !newUser.getEmail().isBlank()) {
            existingUser.setEmail(newUser.getEmail());
            this.validator.validate(existingUser);
            userPrincipal.setEmail(newUser.getEmail());
        }

        if (newUser.getFirstName() != null && !newUser.getFirstName().isBlank()) {
            existingUser.setFirstName(newUser.getFirstName());
            this.validator.validate(existingUser);
            userPrincipal.setFirstName(newUser.getFirstName());
        }

        if (newUser.getLastName() != null && !newUser.getLastName().isBlank()) {
            existingUser.setLastName(newUser.getLastName());
            this.validator.validate(existingUser);
            userPrincipal.setLastName(newUser.getLastName());
        }

        if (newUser.getPassword() != null && !newUser.getPassword().isBlank()) {
            existingUser.setPassword(newUser.getPassword());
            this.validator.validate(existingUser);
            existingUser.setPassword(this.passwordEncoder.encode(newUser.getPassword()));
        }

        this.usersRepository.save(existingUser);
    }

    @Override
    public UserServiceModel getLoggedInUser() {
        MyUserPrincipal userPrincipal = this.getUserPrincipal();
        Optional<User> user = this.usersRepository.findById(userPrincipal.getUserId());
        UserServiceModel mappedUser = this.mapper.map(user.get(), UserServiceModel.class);

        return mappedUser;
    }

    @Override
    public boolean adminExists() {
        UserRole adminRole = this.rolesRepository.findFirstByName("ROLE_ADMIN");
        if (adminRole == null) {
            return false;
        }

        return this.usersRepository.existsByRoles(adminRole);
    }
}