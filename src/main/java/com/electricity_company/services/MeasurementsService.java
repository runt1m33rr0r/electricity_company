package com.electricity_company.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import com.electricity_company.entities.*;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.repositories.*;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.*;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MeasurementsService implements IMeasurementsService {
    private final MeasurementsRepository measurementsRepository;
    private final ClientsRepository clientsRepository;
    private final IValidationService validator;
    private final IMappingUtils mapper;
    private final IPricesService pricesService;
    private final IAuthService authService;

    @Transactional
    @Override
    public void createMeasurement(CreateMeasurementServiceModel measurement) throws InvalidDataException {
        this.validator.validate(measurement);

        Client client = this.clientsRepository.findFirstByUserEmail(measurement.getClientEmail());
        if (client == null) {
            throw new InvalidDataException("Client does not exist!");
        }

        BigDecimal price = new BigDecimal(0);
        PricesServiceModel prices = this.pricesService.getPrices();
        if (client.getType() == ClientType.LEGAL_ENTITY) {
            price = prices.getLegalEntityPrice().multiply(measurement.getAmount());
        } else if (client.getType() == ClientType.PRIVATE_ENTITY) {
            price = prices.getPrivateIndividualPrice().multiply(measurement.getAmount());
        }

        Measurement measurementEntity = new Measurement(measurement.getAmount(), price, LocalDate.now(),
                MeasurementStatus.PENDING, client);
        this.measurementsRepository.save(measurementEntity);

        client.getMeasurements().add(measurementEntity);
        this.clientsRepository.save(client);
    }

    @Override
    public Collection<MeasurementServiceModel> getLoggedInClientMeasurements() throws InvalidDataException {
        UserServiceModel user = this.authService.getLoggedInUser();
        if (user == null) {
            throw new InvalidDataException("User not logged in!");
        }

        Collection<Measurement> measurements = this.measurementsRepository.findByClientUserEmail(user.getEmail());

        return this.mapper.mapAll(measurements, MeasurementServiceModel.class);
    }

    @Override
    public void payMeasurement(long measurementId) throws InvalidDataException {
        Optional<Measurement> measurement = this.measurementsRepository.findById(measurementId);
        if (measurement.isEmpty()) {
            throw new InvalidDataException("Measurement does not exist!");
        }

        if (measurement.get().getStatus() == MeasurementStatus.PAID) {
            throw new InvalidDataException("Measurement already paid!");
        }

        measurement.get().setStatus(MeasurementStatus.PAID);
        this.measurementsRepository.save(measurement.get());
    }
}