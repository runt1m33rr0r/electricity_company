package com.electricity_company.services;

import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Transactional;

import com.electricity_company.entities.*;
import com.electricity_company.repositories.EmployeesRepository;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.*;
import com.electricity_company.utils.contracts.IMappingUtils;
import com.electricity_company.exceptions.InvalidDataException;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EmployeesService implements IEmployeesService {
    private final EmployeesRepository employeesRepository;
    private final IAuthService authService;
    private final IValidationService validator;
    private final IMappingUtils mapper;

    @Override
    @Transactional
    public void createEmployee(EmployeeServiceModel user) throws InvalidDataException {
        if (user == null) {
            throw new InvalidDataException("No user provided!");
        }

        this.validator.validate(user);

        if (this.employeesRepository.existsByUserEmail(user.getEmail())) {
            throw new InvalidDataException("User already exists!");
        }

        User userEntity = this.authService.registerUser(user, Arrays.asList("ROLE_EMPLOYEE"));
        Employee employeeEntity = this.mapper.map(user, Employee.class);
        employeeEntity.setUser(userEntity);
        this.employeesRepository.save(employeeEntity);
    }

    @Override
    public Collection<EmployeeServiceModel> getAll() {
        Collection<EmployeeServiceModel> employees = this.mapper.mapAll(this.employeesRepository.findAll(),
                EmployeeServiceModel.class);

        return employees;
    }

    @Override
    @Transactional
    public void deleteEmployee(String email) throws InvalidDataException {
        if (email == null) {
            throw new InvalidDataException("Invalid data!");
        }

        Employee existingEmployee = this.employeesRepository.findFirstByUserEmail(email);
        if (existingEmployee == null) {
            throw new InvalidDataException("Employee does not exist!");
        }

        existingEmployee.setDeleted(true);
        this.employeesRepository.save(existingEmployee);

        this.authService.deleteUser(email);
    }

    @Override
    public void editEmployee(EditEmployeeServiceModel editEmployee) throws InvalidDataException {
        if (editEmployee == null || editEmployee.getEmail() == null) {
            throw new InvalidDataException("Invalid data!");
        }

        this.validator.validate(editEmployee);

        Employee existingEmployee = this.employeesRepository.findFirstByUserEmail(editEmployee.getEmail());
        if (existingEmployee == null) {
            throw new InvalidDataException("Employee does not exist!");
        }

        existingEmployee.setSalary(editEmployee.getSalary());
        this.employeesRepository.save(existingEmployee);
    }
}