package com.electricity_company.services;

import java.math.BigDecimal;
import java.util.*;

import com.electricity_company.entities.Property;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.repositories.PropertiesReporisoty;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.PropertyServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PropertiesService implements IPropertiesService {
    private final IValidationService validator;
    private final IMappingUtils mapper;
    private final PropertiesReporisoty propertiesReporisoty;

    @Override
    public Collection<PropertyServiceModel> getAll() {
        Collection<PropertyServiceModel> properties = this.mapper.mapAll(this.propertiesReporisoty.findAll(),
                PropertyServiceModel.class);

        return properties;
    }

    @Override
    public void createProperty(PropertyServiceModel propertyServiceModel) throws InvalidDataException {
        if (propertyServiceModel == null) {
            throw new InvalidDataException("No property provided!");
        }

        this.validator.validate(propertyServiceModel);

        if (this.propertiesReporisoty.existsByAddress(propertyServiceModel.getAddress())) {
            throw new InvalidDataException("Property already exists!");
        }

        Property property = this.mapper.map(propertyServiceModel, Property.class);
        this.propertiesReporisoty.save(property);
    }

    @Override
    public void deleteProperty(String propertyAddress) throws InvalidDataException {
        if (propertyAddress == null || propertyAddress == "") {
            throw new InvalidDataException("No property address provided!");
        }

        Property property = this.propertiesReporisoty.findFirstByAddress(propertyAddress);
        if (property == null) {
            throw new InvalidDataException("Property does not exist!");
        }

        property.setDeleted(true);
        this.propertiesReporisoty.save(property);
    }

    @Override
    public void editProperty(PropertyServiceModel propertyServiceModel) throws InvalidDataException {
        if (propertyServiceModel == null) {
            throw new InvalidDataException("No property provided!");
        }

        Optional<Property> property = this.propertiesReporisoty.findById(propertyServiceModel.getId());
        if (!property.isPresent()) {
            throw new InvalidDataException("Property does not exist!");
        }

        if (propertyServiceModel.getAddress() != null && !propertyServiceModel.getAddress().isBlank()) {
            String oldAddress = property.get().getAddress();
            property.get().setAddress(propertyServiceModel.getAddress());
            if (!this.validator.isValid(property.get())) {
                property.get().setAddress(oldAddress);

                throw new InvalidDataException("Address is invalid!");
            }
        }

        if (propertyServiceModel.getMaintenanceCost() != null) {
            BigDecimal oldMaintenanceCost = property.get().getMaintenanceCost();
            property.get().setMaintenanceCost(propertyServiceModel.getMaintenanceCost());
            if (!this.validator.isValid(property.get())) {
                property.get().setMaintenanceCost(oldMaintenanceCost);

                throw new InvalidDataException("Maintenance cost is invalid!");
            }
        }

        this.propertiesReporisoty.save(property.get());
    }
}