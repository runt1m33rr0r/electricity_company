package com.electricity_company.services;

import java.math.BigDecimal;
import java.util.List;

import com.electricity_company.entities.Bills;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.repositories.BillsRepository;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.BillsServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BillsService implements IBillsService {
    private final BillsRepository billsRepository;
    private final IMappingUtils mapper;
    private final IValidationService validator;

    private Bills getBillsData() {
        List<Bills> bills = this.billsRepository.findAll();
        if (bills.size() == 0) {
            return new Bills(new BigDecimal(0));
        }

        return bills.get(0);
    }

    @Override
    public BillsServiceModel getBills() {
        Bills billsData = this.getBillsData();

        return this.mapper.map(billsData, BillsServiceModel.class);
    }

    @Override
    public void setBills(BillsServiceModel bills) throws InvalidDataException {
        Bills existingBills = this.getBillsData();

        if (bills.getProfitBill() != null) {
            existingBills.setProfitBill(bills.getProfitBill());
            this.validator.validate(existingBills);
        }

        this.billsRepository.save(existingBills);
    }
}