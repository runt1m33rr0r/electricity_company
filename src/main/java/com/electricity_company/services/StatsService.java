package com.electricity_company.services;

import java.math.BigDecimal;
import java.util.Collection;

import com.electricity_company.entities.*;
import com.electricity_company.repositories.*;
import com.electricity_company.services.contracts.IStatsService;
import com.electricity_company.services.models.StatsServiceModel;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StatsService implements IStatsService {
    private final MeasurementsRepository measurementsRepository;
    private final EmployeesRepository employeesRepository;
    private final PropertiesReporisoty propertiesReporisoty;
    private final BillsRepository billsRepository;

    @Override
    public StatsServiceModel getStats() {
        BigDecimal income = new BigDecimal(0);
        Collection<Measurement> paidMeasurements = this.measurementsRepository.findByStatus(MeasurementStatus.PAID);
        for (Measurement measurement : paidMeasurements) {
            income = income.add(measurement.getPrice());
        }

        BigDecimal spending = new BigDecimal(0);
        Collection<Employee> employees = this.employeesRepository.findAll();
        Collection<Property> properties = this.propertiesReporisoty.findAll();
        for (Employee employee : employees) {
            spending = spending.add(employee.getSalary());
        }

        for (Property property : properties) {
            spending = spending.add(property.getMaintenanceCost());
        }

        BigDecimal profit = new BigDecimal(0);
        BigDecimal pureProfit = new BigDecimal(0);
        boolean isCompanyProfitable = income.subtract(spending).compareTo(new BigDecimal(0)) > 0;
        if (isCompanyProfitable) {
            profit = income.subtract(spending);

            boolean isBillConfigured = this.billsRepository.findAll().size() > 0;
            if (isBillConfigured) {
                BigDecimal profitBill = this.billsRepository.findAll().get(0).getProfitBill();
                if (profitBill != null) {
                    BigDecimal moneyBilled = profit.multiply(profitBill).divide(new BigDecimal(100));
                    pureProfit = profit.subtract(moneyBilled);
                }
            }
        }

        return new StatsServiceModel(income, spending, profit, pureProfit);
    }
}