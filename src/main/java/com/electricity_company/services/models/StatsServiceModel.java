package com.electricity_company.services.models;

import java.math.BigDecimal;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatsServiceModel {
    private BigDecimal income;
    private BigDecimal spending;
    private BigDecimal profit;
    private BigDecimal pureProfit;
}