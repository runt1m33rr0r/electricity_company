package com.electricity_company.services.models;

import javax.validation.constraints.NotNull;

import com.electricity_company.entities.ClientType;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateClientServiceModel extends UserServiceModel {
    @NotNull
    private ClientType clientType;
}