package com.electricity_company.services.models;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EditEmployeeServiceModel {
    @DecimalMin(value = "0")
    private BigDecimal salary;

    private String email;
}