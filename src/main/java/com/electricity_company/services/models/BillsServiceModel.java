package com.electricity_company.services.models;

import lombok.*;

import java.math.BigDecimal;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillsServiceModel {
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private BigDecimal profitBill;
}