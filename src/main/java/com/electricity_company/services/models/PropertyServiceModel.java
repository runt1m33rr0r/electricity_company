package com.electricity_company.services.models;

import lombok.*;

import java.math.BigDecimal;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PropertyServiceModel {
    private long id;

    @Length(min = 3, max = 30)
    @NotEmpty
    private String address;

    @DecimalMin(value = "0")
    private BigDecimal maintenanceCost;
}