package com.electricity_company.services.models;

import java.math.BigDecimal;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateMeasurementServiceModel {
    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal amount;

    @Length(min = 3, max = 30)
    @NotEmpty
    private String clientEmail;
}