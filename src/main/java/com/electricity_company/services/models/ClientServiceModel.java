package com.electricity_company.services.models;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.electricity_company.entities.ClientType;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientServiceModel extends UserServiceModel {
    @NotNull
    private ClientType clientType;

    @NotNull
    private BigDecimal highestPayment;

    @NotNull
    private BigDecimal amountPaid;

    @NotNull
    private List<MeasurementServiceModel> measurements;
}