package com.electricity_company.services.models;

import java.math.BigDecimal;

import lombok.*;

import javax.validation.constraints.DecimalMin;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PricesServiceModel {
    @DecimalMin(value = "0")
    private BigDecimal privateIndividualPrice;

    @DecimalMin(value = "0")
    private BigDecimal legalEntityPrice;
}