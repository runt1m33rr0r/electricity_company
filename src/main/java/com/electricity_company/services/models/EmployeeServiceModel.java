package com.electricity_company.services.models;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeServiceModel extends UserServiceModel {
    @DecimalMin(value = "0")
    private BigDecimal salary;
}