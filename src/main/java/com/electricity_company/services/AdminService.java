package com.electricity_company.services;

import java.util.Arrays;

import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.AdminServiceModel;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AdminService implements IAdminService {
    private final IValidationService validator;
    private final IAuthService authService;

    @Override
    public void createAdmin(AdminServiceModel admin) throws InvalidDataException {
        if (admin == null) {
            throw new InvalidDataException("No user provided!");
        }

        if (this.authService.adminExists()) {
            throw new InvalidDataException("Admin already exists!");
        }

        this.validator.validate(admin);

        this.authService.registerUser(admin, Arrays.asList("ROLE_ADMIN"));
    }
}