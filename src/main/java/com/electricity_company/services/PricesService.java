package com.electricity_company.services;

import java.math.BigDecimal;
import java.util.List;

import com.electricity_company.entities.Prices;
import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.repositories.PricesRepository;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.PricesServiceModel;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PricesService implements IPricesService {
    private final IValidationService validator;
    private final IMappingUtils mapper;
    private final PricesRepository pricesRepository;

    private Prices getPricesData() {
        List<Prices> prices = this.pricesRepository.findAll();
        if (prices.size() == 0) {
            return new Prices(new BigDecimal(0), new BigDecimal(0));
        }

        return prices.get(0);
    }

    @Override
    public PricesServiceModel getPrices() {
        Prices pricesData = this.getPricesData();

        return this.mapper.map(pricesData, PricesServiceModel.class);
    }

    @Override
    public void setPrices(PricesServiceModel prices) throws InvalidDataException {
        Prices existingPrices = this.getPricesData();
        if (prices.getLegalEntityPrice() != null) {
            existingPrices.setLegalEntityPrice(prices.getLegalEntityPrice());
            this.validator.validate(existingPrices);
        }

        if (prices.getPrivateIndividualPrice() != null) {
            existingPrices.setPrivateIndividualPrice(prices.getPrivateIndividualPrice());
            this.validator.validate(existingPrices);
        }

        this.pricesRepository.save(existingPrices);
    }
}