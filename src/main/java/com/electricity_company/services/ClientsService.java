package com.electricity_company.services;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.electricity_company.entities.*;
import com.electricity_company.repositories.ClientsRepository;
import com.electricity_company.services.contracts.*;
import com.electricity_company.services.models.*;
import com.electricity_company.utils.contracts.IMappingUtils;
import com.electricity_company.exceptions.InvalidDataException;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ClientsService implements IClientsService {
    private final ClientsRepository clientsRepository;
    private final IAuthService authService;
    private final IValidationService validator;
    private final IMappingUtils mapper;

    private Collection<Measurement> getPaidMeasurements(Client client) {
        return client.getMeasurements().stream().filter(c -> c.getStatus() == MeasurementStatus.PAID)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<ClientServiceModel> getAllClients(Optional<BigDecimal> maximumPayment) {
        List<Client> clients = this.clientsRepository.findAll();
        List<ClientServiceModel> mappedClients = new ArrayList<>();
        for (int i = 0; i < clients.size(); i++) {
            Collection<Measurement> payments = this.getPaidMeasurements(clients.get(i));
            if (maximumPayment.isPresent() && payments.size() > 0) {
                long lowerPaymentsCount = payments.stream()
                        .filter(p -> p.getPrice().compareTo(maximumPayment.get()) == -1).count();
                if (lowerPaymentsCount == 0) {
                    continue;
                }
            }

            BigDecimal highestPayment = new BigDecimal(0);
            if (payments.size() > 1) {
                highestPayment = payments.stream().max((first, second) -> first.getPrice().compareTo(second.getPrice()))
                        .get().getPrice();
            } else if (payments.size() == 1) {
                highestPayment = payments.stream().findFirst().get().getPrice();
            }

            BigDecimal paymentsSum = new BigDecimal(0);
            for (Measurement measurement : payments) {
                paymentsSum = paymentsSum.add(measurement.getPrice());
            }

            ClientServiceModel mappedClient = this.mapper.map(clients.get(i), ClientServiceModel.class);
            mappedClient.setAmountPaid(paymentsSum);
            mappedClient.setHighestPayment(highestPayment);
            mappedClients.add(mappedClient);
        }

        return mappedClients;
    }

    @Override
    @Transactional
    public void createClient(CreateClientServiceModel client) throws InvalidDataException {
        if (client == null) {
            throw new InvalidDataException("No user provided!");
        }

        this.validator.validate(client);

        if (this.clientsRepository.existsByUserEmail(client.getEmail())) {
            throw new InvalidDataException("User already exists!");
        }

        User user = this.authService.registerUser(client, Arrays.asList("ROLE_CLIENT"));
        Client clientEntity = this.mapper.map(client, Client.class);
        clientEntity.setUser(user);
        this.clientsRepository.save(clientEntity);
    }

    @Override
    @Transactional
    public void deleteClient(String email) throws InvalidDataException {
        if (email == null) {
            throw new InvalidDataException("Invalid data!");
        }

        Client existingClient = this.clientsRepository.findFirstByUserEmail(email);
        if (existingClient == null) {
            throw new InvalidDataException("Client does not exist!");
        }

        existingClient.getMeasurements().forEach((measurement) -> measurement.setClient(null));
        existingClient.setDeleted(true);
        this.clientsRepository.save(existingClient);

        this.authService.deleteUser(email);
    }
}