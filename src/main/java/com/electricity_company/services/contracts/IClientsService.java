package com.electricity_company.services.contracts;

import com.electricity_company.services.models.*;

import java.math.BigDecimal;
import java.util.*;

import com.electricity_company.exceptions.InvalidDataException;

public interface IClientsService {
    void createClient(CreateClientServiceModel client) throws InvalidDataException;

    void deleteClient(String email) throws InvalidDataException;

    Collection<ClientServiceModel> getAllClients(Optional<BigDecimal> maximumPayment);
}