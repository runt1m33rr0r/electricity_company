package com.electricity_company.services.contracts;

import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.models.PricesServiceModel;

public interface IPricesService {
    PricesServiceModel getPrices();

    void setPrices(PricesServiceModel prices) throws InvalidDataException;
}