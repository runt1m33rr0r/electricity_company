package com.electricity_company.services.contracts;

import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.models.BillsServiceModel;

public interface IBillsService {
    BillsServiceModel getBills();

    void setBills(BillsServiceModel bills) throws InvalidDataException;
}