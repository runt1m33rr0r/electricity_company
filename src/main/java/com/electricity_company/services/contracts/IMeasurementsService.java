package com.electricity_company.services.contracts;

import java.util.Collection;

import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.models.*;

public interface IMeasurementsService {
    void createMeasurement(CreateMeasurementServiceModel measurement) throws InvalidDataException;

    Collection<MeasurementServiceModel> getLoggedInClientMeasurements() throws InvalidDataException;

    void payMeasurement(long measurementId) throws InvalidDataException;
}