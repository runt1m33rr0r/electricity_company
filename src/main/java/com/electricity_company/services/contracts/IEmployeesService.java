package com.electricity_company.services.contracts;

import com.electricity_company.services.models.*;

import java.util.Collection;

import com.electricity_company.exceptions.InvalidDataException;

public interface IEmployeesService {
    void createEmployee(EmployeeServiceModel user) throws InvalidDataException;

    Collection<EmployeeServiceModel> getAll();

    void deleteEmployee(String email) throws InvalidDataException;

    void editEmployee(EditEmployeeServiceModel editEmployee) throws InvalidDataException;
}