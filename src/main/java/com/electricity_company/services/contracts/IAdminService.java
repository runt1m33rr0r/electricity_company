package com.electricity_company.services.contracts;

import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.models.AdminServiceModel;

public interface IAdminService {
    void createAdmin(AdminServiceModel admin) throws InvalidDataException;
}