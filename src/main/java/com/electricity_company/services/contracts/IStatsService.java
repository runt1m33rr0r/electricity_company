package com.electricity_company.services.contracts;

import com.electricity_company.services.models.StatsServiceModel;

public interface IStatsService {
    StatsServiceModel getStats();
}