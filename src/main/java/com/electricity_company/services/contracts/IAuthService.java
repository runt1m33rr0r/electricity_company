package com.electricity_company.services.contracts;

import java.util.Collection;

import com.electricity_company.entities.User;
import com.electricity_company.services.models.UserServiceModel;
import com.electricity_company.exceptions.InvalidDataException;

public interface IAuthService {
    User registerUser(UserServiceModel user, Collection<String> roles) throws InvalidDataException;

    void deleteUser(String email) throws InvalidDataException;

    void modifyUser(String email, UserServiceModel newUser) throws InvalidDataException;

    UserServiceModel getLoggedInUser();

    boolean adminExists();
}