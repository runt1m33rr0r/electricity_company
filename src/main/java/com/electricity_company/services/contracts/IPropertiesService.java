package com.electricity_company.services.contracts;

import java.util.Collection;

import com.electricity_company.exceptions.InvalidDataException;
import com.electricity_company.services.models.PropertyServiceModel;

public interface IPropertiesService {
    void createProperty(PropertyServiceModel propertyServiceModel) throws InvalidDataException;

    Collection<PropertyServiceModel> getAll();

    void deleteProperty(String propertyAddress) throws InvalidDataException;

    void editProperty(PropertyServiceModel propertyServiceModel) throws InvalidDataException;
}