package com.electricity_company.repositories;

import java.util.Collection;

import com.electricity_company.entities.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MeasurementsRepository extends JpaRepository<Measurement, Long> {
    Collection<Measurement> findByClientUserEmail(String email);

    Collection<Measurement> findByStatus(MeasurementStatus status);
}