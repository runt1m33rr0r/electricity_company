package com.electricity_company.repositories;

import com.electricity_company.entities.Property;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertiesReporisoty extends JpaRepository<Property, Long> {
    boolean existsByAddress(String address);

    Property findFirstByAddress(String address);
}