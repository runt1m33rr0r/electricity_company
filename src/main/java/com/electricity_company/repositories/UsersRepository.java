package com.electricity_company.repositories;

import com.electricity_company.entities.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);

    User findFirstByEmail(String email);

    boolean existsByRoles(UserRole role);
}