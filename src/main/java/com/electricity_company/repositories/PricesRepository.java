package com.electricity_company.repositories;

import com.electricity_company.entities.Prices;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PricesRepository extends JpaRepository<Prices, Long> {

}