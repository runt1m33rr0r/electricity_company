package com.electricity_company.repositories;

import com.electricity_company.entities.Bills;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BillsRepository extends JpaRepository<Bills, Long> {

}