package com.electricity_company.config;

import com.electricity_company.utils.MappingUtils;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.springframework.context.annotation.*;

@Configuration
public class ModelMapperConfig {
    private static IMappingUtils mapper;

    static {
        mapper = new MappingUtils();
    }

    @Bean
    public IMappingUtils modelMapper() {
        return mapper;
    }
}