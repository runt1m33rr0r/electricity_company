package com.electricity_company.entities;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.Where;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Where(clause = "is_deleted='false'")
public class Prices extends BaseEntity {
    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal privateIndividualPrice;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal legalEntityPrice;
}