package com.electricity_company.entities;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import org.hibernate.annotations.Where;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Where(clause = "is_deleted='false'")
public class Employee extends BaseEntity {
    @OneToOne
    private User user;

    @DecimalMin(value = "0")
    private BigDecimal salary;
}