package com.electricity_company.entities;

public enum ClientType {
    LEGAL_ENTITY("Legal entity"), PRIVATE_ENTITY("Private entity");

    private final String displayValue;

    private ClientType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}