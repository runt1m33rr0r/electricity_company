package com.electricity_company.entities;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Where;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Where(clause = "is_deleted='false'")
public class Measurement extends BaseEntity {
    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal amount;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal price;

    @NonNull
    private LocalDate measuredOn;

    @NonNull
    private MeasurementStatus status;

    @ManyToOne
    private Client client;
}