package com.electricity_company.entities;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;

import lombok.*;

import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Where(clause = "is_deleted='false'")
public class Property extends BaseEntity {
    @Column(nullable = false)
    @Length(min = 3, max = 30)
    private String address;

    @DecimalMin(value = "0")
    private BigDecimal maintenanceCost;
}