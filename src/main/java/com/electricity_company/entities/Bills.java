package com.electricity_company.entities;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.Where;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Where(clause = "is_deleted='false'")
public class Bills extends BaseEntity {
    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private BigDecimal profitBill;
}