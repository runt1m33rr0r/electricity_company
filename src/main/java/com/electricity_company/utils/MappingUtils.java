package com.electricity_company.utils;

import java.util.*;
import java.util.stream.Collectors;

import com.electricity_company.entities.*;
import com.electricity_company.services.models.*;
import com.electricity_company.utils.contracts.IMappingUtils;

import org.modelmapper.ModelMapper;

public class MappingUtils implements IMappingUtils {
    private final ModelMapper modelMapper;

    public MappingUtils() {
        this.modelMapper = new ModelMapper();

        var clientsTypeMap = this.modelMapper.typeMap(Client.class, ClientServiceModel.class);
        clientsTypeMap.addMappings(mapper -> {
            mapper.map(src -> src.getUser().getEmail(), ClientServiceModel::setEmail);
            mapper.map(src -> src.getUser().getFirstName(), ClientServiceModel::setFirstName);
            mapper.map(src -> src.getUser().getLastName(), ClientServiceModel::setLastName);
        });

        var employeesTypeMap = this.modelMapper.typeMap(Employee.class, EmployeeServiceModel.class);
        employeesTypeMap.addMappings(mapper -> {
            mapper.map(src -> src.getUser().getEmail(), EmployeeServiceModel::setEmail);
            mapper.map(src -> src.getUser().getFirstName(), EmployeeServiceModel::setFirstName);
            mapper.map(src -> src.getUser().getLastName(), EmployeeServiceModel::setLastName);
        });
    }

    public <D, T> D map(final T entity, Class<D> outClass) {
        return this.modelMapper.map(entity, outClass);
    }

    public <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outCLass) {
        return entityList.stream().map(entity -> map(entity, outCLass)).collect(Collectors.toList());
    }

    public <S, D> D map(final S source, D destination) {
        this.modelMapper.map(source, destination);

        return destination;
    }
}