package com.electricity_company.utils.contracts;

import java.util.*;

public interface IMappingUtils {
    <D, T> D map(final T entity, Class<D> outClass);

    <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outCLass);

    <S, D> D map(final S source, D destination);
}